class AssetsController < ApplicationController

  before_action :set_book

  def index
    @assets = @book.assets.order('created_at desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @assets.map{|asset| asset.to_jq_upload } }
    end
  end

  def show
    @asset = @book.assets.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @asset }
    end
  end

  def convert
    @asset = @book.assets.find(params[:asset_id])
    @asset.delay.convert

    respond_to do |format|
      format.json { render json: {message: "Started conversion!"} }
    end
  end

  def create
    @asset = @book.assets.new(asset_params)

    respond_to do |format|
      if @asset.save
        format.html {
          render :json => [@asset.to_jq_upload].to_json,
                 :content_type => 'text/html',
                 :layout => false
        }
        format.json { render json: {files: [@asset.to_jq_upload]}, status: :created, location: book_asset_path(@book, @asset) }
      else
        format.html { render action: "new" }
        format.json { render json: @asset.errors, status: :unprocessable_entity }
      end
    end

  end

  def destroy
    @asset = @book.assets.find(params[:id])
    @asset.destroy

    respond_to do |format|
      format.html { redirect_to asset_url }
      format.json { head :no_content }
    end
  end

  private

  def asset_params
    params.require(:asset).permit(:asset)
  end

  def set_book
    @book = Book.find(params[:book_id])
  end


end
