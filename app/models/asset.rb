class Asset < ActiveRecord::Base
  mount_uploader :asset, AssetUploader
  belongs_to :book

  include Rails.application.routes.url_helpers

  def convert
    epub = self.book.assets.build(converted: true)
    epub.asset = File.open(convert_to_epub)
    epub.save

    html = self.book.assets.build(converted: true)
    html.asset = File.open(convert_to_html)
    html.save

    pdf = self.book.assets.build(converted: true)
    pdf.asset = File.open(convert_to_pdf)
    pdf.save

    system("rm /tmp/*.epub /tmp/*.pdf /tmp/*.html")
    self.converted = true
    self.save
  end




  def to_jq_upload
    {
        "name" => asset.file.filename,
        "size" => asset.size,
        "url" => asset.url,
        "converted" => converted,
        "delete_url" => book_asset_path(self.book, self),
        "convert_url" => book_asset_convert_path(self.book, self),
        "delete_type" => "DELETE"
    }
  end

  private

  def convert_to_epub
    output_filename = "/tmp/#{self.asset.file.filename}.epub"
    system("pandoc #{self.asset.file.path} -s -o #{output_filename}")
    return output_filename
  end

  def convert_to_html
    output_filename = "/tmp/#{self.asset.file.filename}.html"
    system("pandoc -s -S --toc -c /bizdin_kg.css -B /home/dastan/dist/pandoc-tests/bizdin_header.html -A /home/dastan/dist/pandoc-tests/bizdin_footer.html #{self.asset.file.path} -o #{output_filename}")
    return output_filename
  end

  def convert_to_pdf
    output_filename = "/tmp/#{self.asset.file.filename}.pdf"
    system("pandoc -N --template=bizdin.tex --variable mainfont=NotoSerif --variable sansfont=FreeSans --variable monofont=FreeMono --variable fontsize=12pt #{self.asset.file.path} --latex-engine=xelatex --toc -o #{output_filename}")
    return output_filename
  end

end
