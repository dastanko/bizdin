class Book < ActiveRecord::Base

  GENRES = %w(Sci-Fi Fiction Non-Fiction Tragedy Mystery Fantasy Mythology)

  validates :title, :author, :genre, :published_on, presence: true

  validates :image_file_name, allow_blank: true, format: {
    with: /\w+.(gif|jpg|png)\z/i,
    message: 'must reference a GIF, JPG, or PNG image'
  }

  validates :genre, inclusion: { in: GENRES }, unless: 'genre.blank?'

  scope :by, ->(author) { where('author = ?', author) }
  default_scope { order('created_at') }

  has_many :reviews, dependent: :destroy
  has_many :assets, dependent: :destroy

  def average_stars
    if reviews.loaded?
      reviews.map(&:stars).average
    else
      reviews.average(:stars)
    end
  end

  def recent_reviews(recent_count = 2)
    reviews.order('created_at desc').limit(recent_count)
  end
end
