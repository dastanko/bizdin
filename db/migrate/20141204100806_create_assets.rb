class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :asset
      t.integer :book_id

      t.timestamps
    end
  end
end
