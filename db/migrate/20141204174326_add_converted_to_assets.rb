class AddConvertedToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :converted, :boolean, :default => false
  end
end
